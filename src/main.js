// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VueFire from 'vuefire'
import firebase from 'firebase'
import push from './push'

Vue.use(VueFire)

let config = {
    apiKey: "AIzaSyAW6Xjw09zOUy8Xwbbi6oSefF3LD7I_914",
    authDomain: "vue-pwa-d5ba0.firebaseapp.com",
    databaseURL: "https://vue-pwa-d5ba0.firebaseio.com",
    projectId: "vue-pwa-d5ba0",
    storageBucket: "vue-pwa-d5ba0.appspot.com",
    messagingSenderId: "510181125439"
  };
  firebase.initializeApp(config);

push()

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
